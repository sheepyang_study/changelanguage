## 多语言切换
> 1，应用内切换  2，跟随系统切换

#### 测试apk
[app-debug](app-debug.apk)

#### 演示视频
![demo_video.mp4](demo_video.mp4)

#### [教程](https://blankj.com/2019/06/29/only-one-code-change-language/)原文如下
几需一行代码完成多语言切换
发表于 2019-06-29 |  分类于 Android ， 工具类 |  阅读次数:
logo

背景
之前总是有开发者反馈我应用切换了语言，可是工具类获取的 string 却没有发生改变。其实这个问题很简单，你切换语言的 Context 只作用在了你的 Activity 上，并没有对你的 Application 做同样的操作，知道了这点，那么解决问题就很简单了，为了省事，我给大家封装了 LanguageUtils，直接一行代码便可完成多语言的切换，类似微信的语言切换分分钟便可完成。

使用
Gradle:

implementation 'com.blankj:utilcode:latest_version'
APIs
语言相关 -> LanguageUtils.java -> Demo
applySystemLanguage     : 设置系统语言
applyLanguage           : 设置语言
isAppliedLanguage       : 是否设置了语言
getAppliedLanguage      : 获取设置的语言
getContextLanguage      : 获取上下文的语言
getAppContextLanguage   : 获取应用上下文的语言
getSystemLanguage       : 获取系统的语言
updateAppContextLanguage: 更新应用上下文语言
attachBaseContext       : 如果设置语言无效则在 Activity#attachBaseContext 调用它
原理
如果我们的应用不设置 android:configChanges="locale|layoutDirection"，那么应用是跟随系统语言设置的变化而变化的，比如你应用适配了英语（values-en-rUS）和简体中文（values-zh-rCN），那么你去设置里切换成英语的话，返回到你应用中，你的 Activity 会重新创建一遍，把 Activity#Resource#Configuration#Locale 设置为当前系统语言，这样就达到了跟随系统语言设置的变化而变化，但 Application 并没有重启，所以这就导致了一开说到的问题。

要解决跟随系统变化这一点的话，只需要在 Activity#onCreate 的生命周期中把 Application#Resource#Configuration#Locale 设置为系统的 Locale 即可，那么系统的 Locale 怎么读取呢，知道之前屏幕适配方案的人应该也能想到这一方式：Resources.getSystem().getConfiguration().locale，这样，我们的 Application 便也切换成了系统语言，注意更新 Locale 需要兼容下高版本，调用具体代码可以参照如下：

/**
* Update the locale of applicationContext.
*
* @param destLocale The dest locale.
* @param consumer   The consumer.
  */
  public static void updateAppContextLanguage(@NonNull Locale destLocale, @Nullable Utils.Consumer<Boolean> consumer) {
  pollCheckAppContextLocal(destLocale, 0, consumer);
  }

static void pollCheckAppContextLocal(final Locale destLocale, final int index, final Utils.Consumer<Boolean> consumer) {
Resources appResources = Utils.getApp().getResources();
Configuration appConfig = appResources.getConfiguration();
Locale appLocal = getLocal(appConfig);
setLocal(appConfig, destLocale);
// 由于 updateConfigure 并不会立马更新（本地测试小于 1ms 便会更新）config，所以需要后面的轮询来监听变化来做其他处理（比如重启应用）
Utils.getApp().getResources().updateConfiguration(appConfig, appResources.getDisplayMetrics());

    if (consumer == null) return;
    
    if (isSameLocale(appLocal, destLocale)) {
        consumer.accept(true);
    } else {
        if (index < 20) {
            UtilsBridge.runOnUiThreadDelayed(new Runnable() {
                @Override
                public void run() {
                    pollCheckAppContextLocal(destLocale, index + 1, consumer);
                }
            }, 16);
            return;
        }
        Log.e("LanguageUtils", "appLocal didn't update.");
        consumer.accept(false);
    }
}
那么如果是应用内切换语言呢？我们可以仿照系统切换语言的方式，重启我们的应用或者重启所有的 Activity，在打开的 Activity#onCreate 中把 Activity 和 Application 的 Locale 都设置为我们设置的语言即可，当然，这份设置是需要保存下来的，根据你的需求来确定是要保存在服务端还是本地。相关代码如下所示：

private static void applyLanguageReal(final Locale locale,
final boolean isRelaunchApp) {
if (locale == null) {
// 后面的 true 使用的是 sp.commit，因为如果用 apply 的话，杀掉应用重启会来不及保存
UtilsBridge.getSpUtils4Utils().put(KEY_LOCALE, VALUE_FOLLOW_SYSTEM, true);
} else {
UtilsBridge.getSpUtils4Utils().put(KEY_LOCALE, locale2String(locale), true);
}
Locale destLocal = locale == null ? getLocal(Resources.getSystem().getConfiguration()) : locale;
// 这个就是上面提到的函数
updateAppContextLanguage(destLocal, new Utils.Consumer<Boolean>() {
@Override
public void accept(Boolean success) {
if (success) {
restart(isRelaunchApp);
} else {
// use relaunch app
UtilsBridge.relaunchApp();
}
}
});
}

private static void restart(final boolean isRelaunchApp) {
if (isRelaunchApp) {
UtilsBridge.relaunchApp();
} else {
for (Activity activity : UtilsBridge.getActivityList()) {
activity.recreate();
}
}
}

// 工具类调用此函数是在 ActivityLifecycleCallbacks#onActivityCreated 中
static void applyLanguage(final Activity activity) {
String spLocale = UtilsBridge.getSpUtils4Utils().getString(KEY_LOCALE);
if (TextUtils.isEmpty(spLocale)) {
return;
}
Locale destLocal;
if (VALUE_FOLLOW_SYSTEM.equals(spLocale)) {
destLocal = getLocal(Resources.getSystem().getConfiguration());
} else {
destLocal = string2Locale(spLocale);
}
if (destLocal == null) return;
updateConfiguration(activity, destLocal);
updateConfiguration(Utils.getApp(), destLocal);
}

private static void updateConfiguration(Context context, Locale destLocal) {
Resources resources = context.getResources();
Configuration config = resources.getConfiguration();
setLocal(config, destLocal);
resources.updateConfiguration(config, resources.getDisplayMetrics());
}
基于以上分析：

如果应用是跟随系统设置语言来切换的话，那么直接依赖我的工具类即可，它会自动帮你更新 Application 的语言。
如果需要应用内切换语言的话，只需在你切换语言的地方调用 LanguageUtils.applyLanguage(Locale.你要设置的语言[, false]); // 第二个参数可选择性传入，代表是否要重启应用，false 的话是 recreate 所有 Activity，true 的话就是重启应用 即可；
如果需要应用内切换语言变为跟随系统设置语言，那么调用 LanguageUtils.applySystemLanguage([false]); // 参数和上面说的是否重启应用一样 即可。
结语
功能其实很简单，但总是缺少人能把它分析得透彻，从而做得很完美分享出来，希望我这次的分享能让你看到这一点，从而提升你之后的技能。

