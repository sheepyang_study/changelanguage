package com.seuic.changelanguage

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat
import com.blankj.utilcode.util.LanguageUtils
import java.util.Locale

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        findViewById<Button>(R.id.chinese).setOnClickListener {
            LanguageUtils.applyLanguage(Locale.CHINESE, false)
        }

        findViewById<Button>(R.id.english).setOnClickListener {
            LanguageUtils.applyLanguage(Locale.ENGLISH, false)
        }

        findViewById<Button>(R.id.follow_system).setOnClickListener {
            LanguageUtils.applySystemLanguage(false)
        }

    }

}